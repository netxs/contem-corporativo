
!function($) {
    "use strict";

    var Dashboard = function() {};

    //creates Stacked chart
    Dashboard.prototype.createStackedChart  = function(element, data, xkey, ykeys, labels, lineColors) {
        Morris.Bar({
            element: element,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            stacked: true,
            labels: labels,
            hideHover: 'auto',
            barSizeRatio: 0.4,
            resize: true, //defaulted to true
            gridLineColor: '#eeeeee',
            barColors: lineColors
        });
    },


    //creates Donut chart
    Dashboard.prototype.createDonutChart = function(element, data, colors) {
        Morris.Donut({
            element: element,
            data: data,
            resize: true, //defaulted to true
            colors: colors
        });
    },
 
    Dashboard.prototype.init = function() {

        //creating Stacked chart
        var $stckedData  = [
            { y: 'Jan/2017', a: 45, b: 180, c: 100 },
            { y: 'Fev/2017', a: 75,  b: 65, c: 80 },
            { y: 'Mar/2017', a: 100, b: 90, c: 56 },
            { y: 'Abr/2017', a: 75,  b: 65, c: 89 },
            { y: 'Mai/2017', a: 100, b: 90, c: 120 },
            { y: 'Jun/2017', a: 75,  b: 65, c: 110 },
            { y: 'Jul/2017', a: 50,  b: 40, c: 85 },
            { y: 'Ago/2017', a: 75,  b: 65, c: 52 },
            { y: 'Set/2017', a: 50,  b: 40, c: 77 },
            { y: 'Out/2017', a: 75,  b: 65, c: 90 },
            { y: 'Nov/2017', a: 100, b: 90, c: 130 },
            { y: 'Dez/2017', a: 100, b: 90, c: 130 }
        ];
        this.createStackedChart('morris-bar-stacked', $stckedData, 'y', ['a', 'b' ,'c'], ['Bônus de Equipe', 'Bônus Indicação', 'Bônus Bônus Multinível'], ['#3db9dc','#1bb99a', '#777777']);

        //creating donut chart
        var $donutData = [
                {label: "Esquerda", value: 40},
                {label: "Direita", value: 60}
            ];
        this.createDonutChart('morris-donut-example', $donutData, ['#3db9dc','#1bb99a']);
    },
    //init
    $.Dashboard = new Dashboard, $.Dashboard.Constructor = Dashboard
}(window.jQuery),

//initializing
function($) {
    "use strict";
    $.Dashboard.init();
}(window.jQuery);
